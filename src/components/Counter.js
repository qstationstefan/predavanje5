import { useState } from "react";

const Counter = (props) => {
    const [counterValue, setCounterValue] = useState(0);

    const incrementOneHandler = () => {
        setCounterValue(prevState => prevState + 1);
    }

    const incrementNHandler = () => {
        setCounterValue(prevState => prevState + props.incrementStep);
    }

    const removeCounterHandler = () => {
        props.onRemoveCounter(props.id);
    }

    return (
        <div>
            <h2>Counter</h2>
            <div>{counterValue}</div>
            <button onClick={incrementOneHandler}>Increment 1</button>
            <button onClick={incrementNHandler}>{`Increment ${props.incrementStep}`}</button>
            <button onClick={removeCounterHandler}>Remove this counter</button>
        </div>
    );
}

export default Counter;