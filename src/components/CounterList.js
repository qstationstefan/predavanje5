import Counter from "./Counter";
const CounterList = (props) => {
    return (
        <>
            {props.counterIncrements.map(incrementValue => <Counter onRemoveCounter={props.onRemoveCounter} id={incrementValue.id} key={incrementValue.id} incrementStep={incrementValue.incrementStep} />)}
        </>
    )
}

export default CounterList;