import { useState } from "react";

const NewCounterForm = (props) => {
    const [incrementStep, setIncrementStep] = useState("");

    const incrementChangeHandler = (e) => {
        var reg = new RegExp('^[0-9]*$'); // regex koji provjerava da li su uneseni samo brojevi, umjesto ovoga moze bilo koja druga provjera
        if (reg.test(e.target.value)) {
            setIncrementStep(e.target.value)
        }
    }

    const submitHandler = (e) => {
        e.preventDefault();
        props.onAddCounterIncrement(+incrementStep);
    }

    return (
        <form onSubmit={submitHandler}>
            <input onChange={incrementChangeHandler} value={incrementStep} />
            <button type="submit">Add new counter</button>
        </form>
    )
}

export default NewCounterForm;