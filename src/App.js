import { useState } from "react";
import CounterList from "./components/CounterList";
import NewCounterForm from "./components/NewCounterForm";

function App() {
  const [counterIncrements, setCounterIncrements] = useState([]);
  const [pseudoAutoIncrement, setPseudoAutoIncremen] = useState(0);

  const addCounterValueIncrement = (incrementStep) => {
    setCounterIncrements(prevState => [...prevState, { incrementStep, id: pseudoAutoIncrement + 1 }]);
    setPseudoAutoIncremen(prevState => prevState + 1);
  }

  const removeCounterHandler = (id) => {
    setCounterIncrements(prevState => prevState.filter(item => item.id !== id));
  }

  return (
    <>

      <NewCounterForm onAddCounterIncrement={addCounterValueIncrement} />, 
      <CounterList counterIncrements={counterIncrements} onRemoveCounter={removeCounterHandler} />
      </>
  );
}

export default App;
